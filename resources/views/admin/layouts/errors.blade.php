@push('scripts')
	{!! Html::script('assets/admin/demo/default/custom/components/base/toastr.js') !!}
	<script type="text/javascript">
		toastr.options = {
			  "closeButton": false,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": false,
			  "positionClass": "toast-top-right",
			  "preventDuplicates": true,
			  "onclick": null,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "10000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
		};

		@if (session()->has('success'))
			toastr.success("{{ session()->get('success') }}", "Success");
		@elseif (session()->has('failed'))
			toastr.error("{{ session()->get('failed') }}", "Error");
		@elseif (session()->has('notice'))
			toastr.warning("{{ session()->get('notice') }}", "Success");
		@elseif (session()->has('status'))
			toastr.info("{{ session()->get('status') }}", "Success");
		@endif

	</script>

@endpush
