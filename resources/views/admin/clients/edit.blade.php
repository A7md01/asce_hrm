@extends('admin.layouts.index')

@section('content')

<div class="row">
    <div class="col-md-12">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div>
            @endif

            @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
            <form method="POST" action="{{route('clients.update' , $clients->id)}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="put">
            <h3 class="page-title"> تعديل بيانات عميل  </h3>
            <hr>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label> Name </label>
                    <input class="form-control" type="text" name="name" value="{{$clients->name}}">
                    </div>
                </div>
                <div class="col-sm-3">
                        <label>image </label>
                        <input type="file" class="form-control" name="image" >
                        <img src = " {{asset('images/' . $clients->image)}}">
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Client ID</label>
                        <input class="form-control" name="client_id" type="text" value="{{$clients->client_id}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Email</label>
                        <input class="form-control" type="email" name="email" value="{{$clients->email}}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input class="form-control" type="text" name="phone" value="{{$clients->phone}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Job Tilte</label>
                        <input class="form-control" name="job_title" type="text" value="{{$clients->job_title}}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Company Name</label>
                        <input class="form-control" type="text" name="company_name" value="{{$clients->company_name}}">
                    </div>
                </div>
            </div>
            <div class="submit-section">
                <button class="btn btn-primary submit-btn">Save Change</button>
            </div>
        </form>
    </div>
</div>

@endsection
