@extends('admin.layouts.index')
@section('content')

    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Title -->
        <div class="row">
            <div class="col-sm-5 col-5">
                <h4 class="page-title">الأقسام</h4>
            </div>
            <div class="col-sm-7 col-7 text-right m-b-30">
                <a href="{{ route('departments.create') }}" class="btn add-btn" ><i class="fa fa-plus"></i>إضافة قسم </a>
            </div>
        </div>
        <!-- /Page Title -->
        <div class="row">
            <div class="col-md-12">
                <div>
                    <table class="table table-striped custom-table mb-0 datatable">
                        <thead>
                            <tr>
                                <th style="width: 40px;">#</th>
                                <th style="width: 80% ; font-size: 120%">أسم القسم</th>
                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($departments as $department)
                            <tr>
                                <td>{{$department->id}}</td>
                                <td> {{$department->name}}</td>
                                <td>
                                    <form method="post" action="{{route('departments.destroy', $department->id)}}">
                                        <span>
                                            <a href="{{ route('departments.edit', $department->id) }}"><i class="btn btn-primary"  >تعديل </i></a>
                                        </span>
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <div class="btn-group">
                                                <button  onclick="return confirm(' هل انت متاكد ؟؟')" class="btn btn-danger" type="submit">حذف</button>
                                            </div>
                                    </form>
                                        </span>
                                    </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->

@endsection

