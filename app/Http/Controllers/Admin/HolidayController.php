<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Holiday;

class HolidayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $holidays = Holiday::all();
        return view('admin.holidays.index' ,compact('holidays'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $holidays = Holiday::all();
        return view('admin.holidays.create' ,compact('holidays'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'  =>'required',
            'holiday_date'  =>'required',
            'day' => 'required',
        ]);
            $holidays = new Holiday([
            'title'             => $request->title,
            'holiday_date'      => $request->holiday_date,
            'day'               => $request->day
            ]);
            $holidays->save();
            return Redirect('admin/holidays');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $holidays = Holiday::find($id);
        return view('admin.holidays.edit',compact('holidays'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $holidays = Holiday::find($id);
        $holidays->update([
            $holidays->title = $request->title,
            $holidays->holiday_date = $request->holiday_date,
            $holidays->day = $request->day,
        ]);
        $holidays->save();
        return Redirect('admin/holidays');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $holidays = Holiday::find($id);
        $holidays->delete();
        return Redirect('admin/holidays');
    }
}
