
<!DOCTYPE html>

<html lang="en" >
    <head>
        <meta charset="utf-8" />
        <title>
            {{ env('APP_NAME') }} | Login Page
        </title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

        {{ HTML::style('assets/admin/vendors/base/vendors.bundle.css') }}
        {{ HTML::style('assets/admin/demo/default/base/style.bundle.css') }}
        <link rel="shortcut icon" href="" />
    </head>
    <body class="m--skin-m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" style="background-image: url(http://159.89.229.171/images/media/bg/bg-3.jpg);">
                <div class="m-grid__item m-grid__item--fluid    m-login__wrapper">
                    <div class="m-login__container">

                        <div class="m-login__logo" >
                            <a href="#">
                                <img style="height: 100px;" src="{{ asset('\assets\admin\demo\default\media\img\logo\medex_logo.png') }}">
                            </a>
                        </div>
                        <div class="m-login__signin" style="margin-top: -20px">
                            <div class="m-login__head">
                                <h3 class="m-login__title">
                                    Sign In To Admin
                                </h3>
                            </div>
                            <form class="m-login__form m-form" method="POST" action="{{ route('admin.login') }}">

                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="role" style="font-size: 18px">
                                        Sign in as
                                    </label>
                                </div>
                                <div class="form-group m-form__group">
                                    <input id="email" type="email" class="form-control m-input" name="email" value="{{ old('email') }}" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group m-form__group">
                                    <input id="password" type="password" class="form-control m-input m-login__form-input--last" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="row m-login__form-sub">
                                    <div class="col m--align-left m-login__form-left">
                                        <label class="m-checkbox  m-checkbox--focus">
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="m-login__form-action">
                                    <button  type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                                        Sign In
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </body>
    <!-- end::Body -->
</html>
