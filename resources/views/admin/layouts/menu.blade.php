<!-- Sidebar -->
<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li>
                    <a href="{{ url('/admin') }}"><i class="la la-dashboard"></i> <span style="font-size: 140%">الرئيسية</span></a>
                </li>
                <li class="submenu">
                    <a href="#" ><i class="la la-user"></i> <span style="font-size: 120%"> الموظفيين </span> <span class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li><a href="{{ route('employees.index') }}" style="font-size: 110%">جميع الموظفيين </a></li>
                        <li><a href="{{ route('departments.index') }}" style="font-size: 110%">الأقسام</a></li>
                        <li><a href="{{ route('designations.index') }}" style="font-size: 110%">الوظائف</a></li>
                        <li><a href="{{ route('holidays.index') }}" style="font-size: 110%">الأجازات الرسمية</a></li>
                    </ul>
                    <li>
                        <a href="{{ route('clients.index') }}" ><i class="la la-users"></i> <span style="font-size: 120%"> العملاء </span></a>
                    </li>
                    <li>
                            <a href="{{ route('company_profiles.index') }}" ><i class="la la-building"></i> <span style="font-size: 120%"> بيانات الشركة </span></a>
                        </li>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- /Sidebar -->
