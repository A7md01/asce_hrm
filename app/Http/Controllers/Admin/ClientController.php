<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client ;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB ;


class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $clients = Client::all();
         $search = $request->get('search');
        $c = DB::table('clients')->where('name' , 'like' , '%'.$search.'%');
        return view('admin.clients.index',compact('clients' , 'search' , 'c'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::all();
        return view('admin.clients.create',compact('clients'));
    }
    public function search(Request $request){

        // return Redirect('admin/clients');
        return view('admin.clients.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'           =>'required',
            'image'          => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'email'          =>'required',
            'phone'          =>'required',
            'client_id'      => 'required',
            'company_name'   =>'required',
            'job_title'      => 'required'
        ]);

            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);
            $clients = new Client([
                'name'            => $request->name,
                'image'           =>  $imageName,
                'email'           => $request->email,
                'phone'           => $request->phone,
                'client_id'       => $request->client_id,
                'company_name'    => $request->company_name,
                'job_title'       =>  $request->job_title
            ]);
            $clients->save();
            // return 'fdf';
            return Redirect('admin/clients');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clients =Client::find($id);
        return view('admin.clients.edit',compact('clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images'), $imageName);
        $clients = Client::find($id);
        $clients->update([
            $clients->name = $request->name,
            $clients->image = $imageName,
            $clients->email = $request->email,
            $clients->phone = $request->phone,
            $clients->client_id = $request->client_id,
            $clients->company_name = $request->company_name,
            $clients->job_title = $request->job_title

        ]);
        $clients->save();
        return Redirect('admin/clients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $clients = Client::find($id);
        $clients->delete();
        return Redirect('admin/clients');
    }
}
