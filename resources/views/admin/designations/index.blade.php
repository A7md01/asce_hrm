@extends('admin.layouts.index')

@section('content')
<!-- Page Content -->
<div class="content container-fluid">

    <!-- Page Title -->
    <div class="row">
        <div class="col-sm-5 col-5">
            <h4 class="page-title">الوظائف</h4>
        </div>
        <div class="col-sm-7 col-7 text-right m-b-30">
            <a href="{{route('designations.create')}}" class="btn add-btn"><i class="fa fa-plus"></i> إضافة وظيفة</a>
        </div>
    </div>
    <!-- /Page Title -->

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped custom-table mb-0 datatable">
                    <thead>
                        <tr>
                            <th style="width: 30px;">#</th>
                            <th style="font-size: 120%">الوظيفة </th>
                            <th style="font-size: 120%">القسم </th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($designations as $designation)
                            <tr>
                                <td>{{$designation->id}}</td>
                                <td>{{$designation->name}}</td>
                                {{-- <td>{{ App\Department::find($designation->department_id)->name}}</td> --}}
                                <td>{{$designation->department->name}}</td>
                                <td class="text-right">
                                    <form method="post" action="{{route('designations.destroy', $designation->id)}}">
                                       {{csrf_field()}}
                                       <span>
                                            <a href="{{ route('designations.edit', $designation->id) }}">
                                                <i class="btn btn-primary" >تعديل </i>
                                            </a>
                                        </span>
                                                <input type="hidden" name="_method" value="DELETE" />
                                                <div class="btn-group">
                                                    <button  onclick="return confirm(' هل انت متاكد ؟؟')" class="btn btn-danger" type="submit">حذف</button>
                                                </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /Page Content -->





@endsection

