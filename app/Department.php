<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Designation;
class Department extends Model
{
    protected $fillable = ['name'];
    public function Designation(){
        
        return $this->hasMany('App\Models\Designation', 'department_id');
    }
}
