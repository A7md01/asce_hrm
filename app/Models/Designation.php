<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Department;
class Designation extends Model
{
    protected $fillable = ['name' , 'department_id'];
    public function Department(){
        return $this->belongsTo('App\Department' , 'department_id');
    }
}
