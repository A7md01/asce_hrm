<?php

namespace App\Notifications;

use App\Models\Notification;
use App\Models\User;
use Edujugon\PushNotification\PushNotification;

trait BaseNotifications {

    public function sendNotification($user_id, $order_id, $type) {
        $user = User::find($user_id);
        Notification::create(['user_id' => $user_id, 'order_id' => $order_id, 'type' => $type]);

        if ($type == 1) {
            $this->sendToUser($user, ' New order assigned to you ', $type, $order_id);
        } elseif ($type == 2) {
            $this->sendToUser($user, ' Your order is accepted ', $type, $order_id);
        } elseif ($type == 3) {
            $this->sendToUser($user, ' Your order is rejected', $type, $order_id);
        } elseif ($type == 4) {
            $this->sendToUser($user, ' New order assigned to you', $type, $order_id);
        }
    }

    public function sendToUser($user, $message, $type, $order_id) {

            $push = new PushNotification('fcm');
            $msg = [
                'data' => [
                    'title' => "'Medex App'",
                    'body' => "'$message'",
                    'sound' => 'default',
                    'type' => $type,
                    'order_id' => $order_id,
                ],

            ];


            foreach ($user->device_tokens as $user) {
                $p = $push->setMessage($msg)
                    ->setDevicesToken($user->device_token)
                    ->send();

            }


    }

}
