@extends('admin.layouts.index')
@section('content')

<!-- Page Content -->
<div class="content container-fluid">

        <!-- Page Title -->
        <div class="row">
            <div class="col-sm-5 col-5">
                <h4 class="page-title"> أجازات 2019</h4>
            </div>
            <div class="col-sm-7 col-7 text-right m-b-30">
                <a href="{{route('holidays.create')}}" class="btn add-btn" ><i class="fa fa-plus"></i> إضافة اجازة  </a>
            </div>
        </div>
        <!-- /Page Title -->

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped custom-table mb-0">
                        <thead>
                            <tr>
                                {{-- <th>#</th> --}}
                                <th style="font-size: 120%"> المناسبة </th>
                                <th style="font-size: 120%"> التاريخ  </th>
                                <th style="font-size: 120%"> اليوم  </th>

                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($holidays as $holiday)
                            <tr class="holiday-upcoming">
                                <td>{{ $holiday->title }}</td>
                                <td>{{ $holiday->holiday_date }}</td>
                                <td>{{ $holiday->day }}</td>
                                <td class="text-right">
                                    <form method="post" action="{{route('holidays.destroy', $holiday->id)}}">
                                        <span>
                                            <a href="{{ route('holidays.edit', $holiday->id) }}"><i class="btn btn-primary"  >تعديل </i></a>
                                        </span>
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <div class="btn-group">
                                                <button  onclick="return confirm(' هل انت متاكد ؟؟')" class="btn btn-danger" type="submit">حذف</button>
                                            </div>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->


@endsection

