<?php

use Spatie\Permission\Models\Permission;
use Carbon\Carbon;

define('ADMIN_PATH', 'admin');
define('FRONT_PATH', 'front');

Route::get('/test', function () {
$now =time();
$oneYearFfromNow = date('Y-M-d',$now);
dd( $oneYearFfromNow);
});

Route::get('/add_perm', function () {
});

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/', function () {
    return view('home');
});

Auth::routes();


Route::group(['prefix' => ADMIN_PATH], function () {

    # admin authentication area
    Route::group(['namespace' => 'Auth\Admin'], function () {
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login')->name('admin.login');
        Route::get('logout', 'LoginController@logout')->name('admin-logout');

        # admin reset password routes
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
        Route::post('password/reset', 'ResetPasswordController@reset');
        Route::get('password/reset/{token}', 'ResetPasswordController@showLinkRequestForm')->name('admin.password.reset');
    }); # end of admin auth area

    # admin namespace area
    Route::group(['namespace' => 'Admin', 'middleware' => 'auth:admin'], function () {

        Route::get('/', 'DashboardController@index')->name('admin.dashboard');

        Route::resource('/admin', 'AdminController');
        Route::resource('/employees', 'EmployeeController');
        Route::resource('/departments', 'DepartmentController');
        Route::resource('/designations', 'DesignationController');
        Route::resource('/clients', 'ClientController');
        Route::resource('/holidays' , 'HolidayController');
        Route::resource('/company_profiles' , 'CompanyProfileController');

    }); # end of admin namespace
}); # end of admin area
