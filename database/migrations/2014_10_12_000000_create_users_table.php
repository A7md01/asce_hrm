<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('pharmacy_name')->nullable();
            $table->integer('type');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->string('password');
            $table->string('address')->nullable();
            $table->string('responsible_dr')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('image')->nullable();
            $table->integer('balance')->default(0);
            $table->integer('contractor_id')->nullable();
            $table->date('activated_at')->nullable();
            $table->string('activation');
            $table->string('reset_password_code')->default(1);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
