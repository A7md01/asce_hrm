<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <meta name="description" content="Smarthr - Bootstrap Admin Template">
		<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
        <meta name="author" content="Dreamguys - Bootstrap Admin Template">
        <meta name="robots" content="noindex, nofollow">
        <title>ASCE-HR</title>

		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('assets/admin/plugins/bootstrap-rtl/css/bootstrap.min.css') }}">

		<!-- Fontawesome CSS -->
        <link rel="stylesheet" href="{{ asset('assets/admin/css/font-awesome.min.css') }}">

		<!-- Lineawesome CSS -->
        <link rel="stylesheet" href="{{ asset('assets/admin/css/line-awesome.min.css') }}">

		<!-- Main CSS -->
        <link rel="stylesheet" href="{{ asset('assets/admin/css/style.css') }}">

        <script src="{{ asset('assets/admin/js/html5shiv.min.js') }}"></script>
        <script src="{{ asset('assets/admin/js/respond.min.js') }}"></script>

    </head>
    <body>
		<!-- Main Wrapper -->
        <div class="main-wrapper">

            @include('admin.layouts.header')

            @include('admin.layouts.menu')

			<!-- Page Wrapper -->
            <div class="page-wrapper">

				<!-- Page Content -->
                <div class="content container-fluid">

					<!-- Content Starts -->
                    @include('admin.layouts.errors')

                    @yield('content')
					<!-- /Content End -->

                </div>
				<!-- /Page Content -->

            </div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->

		<!-- Sidebar Overlay -->
		<div class="sidebar-overlay" data-reff=""></div>

		<!-- jQuery -->
        <script src="{{ asset('assets/admin/js/jquery-3.2.1.min.js') }}"></script>

		<!-- Bootstrap Core JS -->
        <script src="{{ asset('assets/admin/js/popper.min.js') }}"></script>
        <script src="{{ asset('assets/admin/plugins/bootstrap-rtl/js/bootstrap.min.js') }}"></script>

		<!-- Slimscroll JS -->
		<script src="{{ asset('assets/admin/js/jquery.slimscroll.min.js') }}"></script>

		<!-- Custom JS -->
		<script src="{{ asset('assets/admin/js/app.js') }}"></script>

    </body>
</html>
