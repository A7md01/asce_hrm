@extends('admin.layouts.index')

@section('content')

<div class="row">
    <div class="col-md-12">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div>
            @endif

            @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
        <form method="POST" action="{{route('holidays.update' , $holidays->id)}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="put">
            <h3 class="page-title">تعديل القسم </h3>
            <hr>
            <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label style="font-size: 120%"> اسم الاجازة </label>
                        <input class="form-control" type="text" name="title" value="{{$holidays->title}}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                            <label style="font-size: 120%">تاريخ الاجازة </label>
                            <input type="date" class="form-control" name="holiday_date" value="{{$holidays->holiday_date}}">
                    </div>
                    <div class="col-sm-4">
                            <div class="form-group">
                                <label style="font-size: 120%"> اليوم </label>
                                <input class="form-control" type="text" name="day" value="{{$holidays->day}}">
                            </div>
                        </div>
                </div>

            <div class="submit-section">
                <button class="btn btn-primary submit-btn">حفظ</button>
            </div>
        </form>
    </div>
</div>

@endsection
