<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyProfile extends Model
{
    protected $filable = ['name' ,
    'address' ,
      'logo' ,
      'phone',
      'email' ,
      'website',
      'ceo_name',
      'country',
      'city',
      'postal_code',
      'fax',
      'mobile'];

}
