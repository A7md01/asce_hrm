@extends('admin.layouts.index')
@section('content')

<!-- Page Content -->
<div class="content container-fluid">
        <!-- Page Title -->
        <div class="row">
            <div class="col">
                <h4 class="page-title">العملاء</h4>
            </div>
            <div class="col-12 text-right m-b-30">
             <a href="{{route('clients.create')}}" class="btn add-btn"><i class="fa fa-plus"></i> إضافة عميل</a>
            </div>
        </div>
        <!-- Page Title -->
        <!-- Search Filter -->
        <form action="{{route('clients.index')}}" method="GET">
                {{csrf_field()}}
            <div class="row filter-row">
                <div class="col-sm-6 col-md-6">
                    <div class="form-group form-focus">
                        <input type="search" class="form-control floating" name="search">
                        <label class="focus-label">الأسم </label>
                    </div>
                </div>
                <div class="col-sm-6 col-md-2">
                    <button type="submit" class="btn btn-success btn-block" style="font-size: 110%"> بحث </button>
                </div>
            </div>
        </form>
        <!-- Search Filter -->

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped custom-table datatable">
                        <thead>
                            <tr>
                                <th>ألاسم</th>
                                <th>Client ID</th>
                                <th>أسم الشركة </th>
                                <th>الأيميل</th>
                                <th>رقم التليفون</th>
                                <th>الوظيفة</th>
                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <div class="form-group row">
                            {{-- <div class="col-10">
                                <input class="form-control" type="date">
                            </div> --}}
                        <tbody>
                            @foreach ($clients as $client)
                            <tr>
                                <td>
                                    <h2 class="table-avatar">
                                        <a class="avatar"><img src="{{ asset('images/'.$client->image) }}" alt=""></a>
                                        <a>{{ $client->name }}</a>
                                    </h2>
                                </td>
                                <td>{{$client->client_id}}</td>
                                <td> {{$client->company_name}}</td>
                                <td>{{ $client->email}}</td>
                                <td>{{ $client->phone}}</td>
                                <td>{{ $client->job_title}}</td>
                                <td class="text-right">
                                    <form method="post" action="{{route('clients.destroy', $client->id)}}">
                                        <span>
                                            <a href="{{ route('clients.edit', $client->id) }}"><i class="btn btn-primary"  >تعديل </i></a>
                                        </span>
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <div class="btn-group">
                                                <button  onclick="return confirm(' هل انت متاكد ؟؟')" class="btn btn-danger" type="submit">حذف</button>
                                            </div>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->


@endsection

