<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['name', 'image', 'email','phone','client_id','company_name','job_title'];
}
