<?php

namespace App\Models;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /*
     * $type == 1 ? pharmacy
     * $type == 2 ? delivery
     * $type == 3 ? contracting representative
     * */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];


    public static function boot()
    {
        parent::boot();
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function setImageAttribute()
    {
        $image = request()->file('image')->store('uploads/'.date('Y-m-d'));
        $this->attributes['image'] = $image;
    }
}
