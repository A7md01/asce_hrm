$(document).ready(function() {

   $(".mb-signout").on("click",function(){
         $("#mb-signout").addClass("open");
       return true;
    });
   $(".mb-client").on("click",function(){
         $("#mb-client").addClass("open");
       return true;
    });
   $(".mb-control-close").on("click",function(){
       $(this).parents(".message-box").removeClass("open");
       return true;
    });
    $(document).on('submit','.deleteRow', function(event) {
        var box = $("#mb-remove-row");
        box.addClass("open");

        box.find(".mb-control-yes").on("click",function(){
            box.removeClass("open");
            $("#"+row).hide("slow",function(){
               // $(this).remove();

        event.preventDefault();
        var thisButton = $(this);
        var formAction = thisButton.attr('action');
        var formMethod = thisButton.attr('method');
        var csrf = $('input[name="_token"]').val();
                // $.ajax({
                //     url: formAction,
                //     type: 'delete',
                //     dataType: 'json',
                //     data: {"_token":csrf},
                //
                //     success:function(data){
                //        if(data.status == 'true')
                //             {
                //              $(thisButton).parent().parent().hide(500);
                //               $('.successMessage').html(
                //               '<div class="panel bg-success pos-rlt"><span class="arrow bottom b-success"></span><div class="panel-body">'
                //                   +data.message+'</div></div>');
                //                 // swal(data.message, "", "success");
                //
                //             }
                //             else
                //             {
                //                  $('.successMessage').html(
                //               '<div class="panel bg-danger pos-rlt"><span class="arrow bottom b-danger"></span><div class="panel-body">'
                //                   +data.message+'</div></div>');
                //     // swal(data.message, "", "success");
                //
                //             }
                //
                //     },
                // });
    }); //delete
    });
});

/****************************************Delete File*************************************/
$(document).on('submit','.deleteFileRow', function(event) {
        event.preventDefault();
        var form = $(this);
        var formAction = form.attr('action');
        var formMethod = form.attr('method');
        var csrf = $('input[name="_token"]').val();
        // $.ajax({
        //     url: formAction,
        //     type: 'delete',
        //     dataType: 'json',
        //     data: {"_token":csrf},
        //
        //     success:function(data){
        //        if(data.status == 'true')
        //         {
        //         $(form).parent().parent().hide(500);
        //           // $('.successMessage').html(
        //           // '<div class="panel bg-success pos-rlt"><span class="arrow bottom b-success"></span><div class="panel-body">'
        //           //     +data.message+'</div></div>');
        //             swal(data.message, "", "success")
        //
        //         }
        //         else
        //         {
        //             swal(data.message, "", "success")
        //
        //           //    $('.successMessage').html(
        //           // '<div class="panel bg-danger pos-rlt"><span class="arrow bottom b-danger"></span><div class="panel-body">'
        //           //     +data.message+'</div></div>');
        //         }
        //
        //     },
        // });
    }); //delete

/***************************************************************************************/


$('.ajax-form-request').on('submit',function(event){
    event.preventDefault();
    $('.ajaxMessage').remove();
    $('*').removeClass('has-error');

    var thisForm   = $(this) ;
    var formAction = thisForm.attr('action');
    var formMethod = thisForm.attr('method');
        // var formData   =  thisForm.serialize();
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var formData    = new FormData(this);

        $.ajax({
            url: formAction,
            type: formMethod,
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $(thisForm).find('#submit').append(' <i class="fa fa-spinner fa-pulse"></i> ').attr('disabled','disabled');

            },
            success: function(data){
                if(data.status == 'true')
                {
                    swal(data.message, "", "success")
                    $(thisForm).trigger("reset");
                    $("#buildyourform").empty();
                    for (instance in CKEDITOR.instances) {
                        CKEDITOR.instances[instance].setData("")
                    }
                }
                else
                {
                    thisForm.parent().prepend('<div class="alert alert-danger ajaxMessage">'+data.message+'</div>');
                }

            },
            error:function(data){

                var errors = data.responseJSON;

                $('#link-v-home').trigger('click');
                $.each(errors['errors'], function(index, val) {

                    index = index.replace('.*','[]')
                   console.log(index);
                   try{
                       $(thisForm).find('[name='+''+index+''+']').parent().addClass('has-error');
                       $(thisForm).find('[name='+''+index+''+']').parent().append('<small class="text-danger ajaxMessage">'+val+'</small>');
                   }
                   catch(err){
                    console.log('test')
                }
            });

            },
            complete: function(){
                $(thisForm).find('#submit').find('.fa').remove();
                $(thisForm).find('#submit').prop('disabled',false);
            },

        });//ajax

        return false;


    });//ajax-form-request

/***************************************************************************************/
$('.ajax-form-request-model').on('submit',function(event){
        event.preventDefault();
        $('.ajaxMessage').remove();
        $('*').removeClass('has-error');

        var thisForm   = $(this) ;
        var formAction = thisForm.attr('action');
        var formMethod = thisForm.attr('method');
        // var formData   =  thisForm.serialize();
        var formData    = new FormData(this);

        $.ajax({
            url: formAction,
            type: formMethod,
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $(thisForm).find('#submit').append(' <i class="fa fa-spinner fa-pulse"></i> ').attr('disabled','disabled');

            },
            success: function(data){
                if(data.status == 'true')
                {
                    // thisForm.parent().prepend('<div class="alert alert-success text-center"role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><strong>'
                    //               +data.message+'</strong></div>');
                    swal(data.message, "", "success")
                    $(thisForm).trigger("reset");
                    $('.pass').show();
                    $('.data').empty()
                       $('#con-close-modal').modal('toggle');
                    $('.modal-backdrop').remove()
                    $.ajax({
                        url: window.location.href,
                        type: 'get',
                        dataType: 'json',
                        success: function(data) {
                            $('.data').html(data);
                        },
                    });
                    $("#buildyourform").empty();

                }
                else
                {
                    thisForm.parent().prepend('<div class="alert alert-danger ajaxMessage">'+data.message+'</div>');
                }

            },
            error:function(data){
                var errors = data.responseJSON;
                $.each(errors['errors'], function(index, val) {
                     index = index.replace('.*','[]')
                     $(thisForm).find('[name='+''+index+''+']').parent().addClass('has-error');
                     $(thisForm).find('[name='+''+index+''+']').parent().append('<small class="text-danger ajaxMessage">'+val+'</small>');
                });

            },
            complete: function(){
                $(thisForm).find('#submit').find('.fa').remove();
                $(thisForm).find('#submit').prop('disabled',false);
            },

        });//ajax

        return false;


    });//ajax-form-request
/*********************************************************************************/

$('.update-ajax-form-request').on('submit',function(event){
        event.preventDefault();
        $('.ajaxMessage').remove();
        $('*').removeClass('has-error');
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
        var thisForm   = $(this) ;
        var formAction = thisForm.attr('action');
        var formMethod = thisForm.attr('method');
        var formData    = new FormData(this);

        var hasEnctype = thisForm.attr('enctype');
        $.ajax({
            url: formAction,
            type: formMethod,
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){
                 $(thisForm).find('#submit').append(' <i class="fa fa-spinner fa-pulse"></i> ').attr('disabled','disabled');

            },
            success: function(data){
                if(data.status == 'true')
                {
                    // thisForm.parent().prepend('<div class="alert alert-success text-center"role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><strong>'
                    //               +data.message+'</strong></div>');
                    swal(data.message, "", "success")
                    $('.pass').show();
                    if (typeof hasEnctype !== typeof undefined && hasEnctype !== false) {
                        //window.location.reload();
                    }
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].setData("")
                }
                }
                else
                {
                    thisForm.parent().prepend('<div class="alert alert-danger ajaxMessage">'+data.message+'</div>');
                }

            },
            error:function(data){
                var errors = data.responseJSON;
                $.each(errors['errors'], function(index, val) {
                         index = index.replace('.*','[]')
                      if(index  == 'expire_date')
                    {
                          $(thisForm).find('[name='+''+index+''+']').parent().addClass('has-error');
                          $(thisForm).find('[name='+''+index+''+']').parent().parent().append('<small class="text-danger ajaxMessage">'+val+'</small>');
                    }
                     else
                         {
                              $(thisForm).find('[name='+''+index+''+']').parent().addClass('has-error');
                             $(thisForm).find('[name='+''+index+''+']').parent().append('<small class="text-danger ajaxMessage">'+val+'</small>');
                         }
                });

            },
            complete: function(){
                $(thisForm).find('#submit').find('.fa').remove();
                $(thisForm).find('#submit').prop('disabled',false);
            },

        });//ajax

        return false;


    });//update-ajax-form-request
       /*********************************************************************************/

$(document).on('submit','.update-ajax-form-request-model',function(event){
        event.preventDefault();
        $('.ajaxMessage').remove();
        $('*').removeClass('has-error');

        var thisForm   = $(this) ;
        var formAction = thisForm.attr('action');
        var formMethod = thisForm.attr('method');
        var formData    = new FormData(this);

        var hasEnctype = thisForm.attr('enctype');
        $.ajax({
            url: formAction,
            type: formMethod,
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){
                 $(thisForm).find('#submit').append(' <i class="fa fa-spinner fa-pulse"></i> ').attr('disabled','disabled');

            },
            success: function(data){
                if(data.status == 'true')
                {
                    // thisForm.parent().prepend('<div id="success-alert" class="alert alert-success text-center"role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><strong>'
                    //               +data.message+'</strong></div>');
                    swal(data.message, "", "success")
                             $('.data').empty()
                             $('.modal-backdrop').remove()

                    $.ajax({
                        url: window.location.href,
                        type: 'get',
                        dataType: 'json',
                        success: function(data) {
                             $('.data').empty()
                            $('.data').html(data);
                        },
                    });
                }
                else
                {
                    thisForm.parent().prepend('<div class="alert alert-danger ajaxMessage">'+data.message+'</div>');
                }

            },
            error:function(data){
                var errors = data.responseJSON;
                $.each(errors, function(index, val) {
                     index = index.replace('.*','[]')
                      if(index  == 'expire_date')
                    {
                          $(thisForm).find('[name='+''+index+''+']').parent().addClass('has-error');
                          $(thisForm).find('[name='+''+index+''+']').parent().parent().append('<small class="text-danger ajaxMessage">'+val+'</small>');
                    }
                     else
                         {
                              $(thisForm).find('[name='+''+index+''+']').parent().addClass('has-error');
                             $(thisForm).find('[name='+''+index+''+']').parent().append('<small class="text-danger ajaxMessage">'+val+'</small>');
                         }
                });

            },
            complete: function(){
                $(thisForm).find('#submit').find('.fa').remove();
                $(thisForm).find('#submit').prop('disabled',false);
            },

        });//ajax

        return false;


    });//update-ajax-form-request


/*************************************************************************************/
    $(document).on('click','.pagination a',function(event){
        event.preventDefault();
          var str =window.location.href;
var res = str.split("?");
        var pageinate = $(this).attr('href')+"&"+res[1];
        $.get(pageinate, function(data) {
            $('.data').html(data);
        });

        return false;
    });//pagination

/*************************************************************************************/

    $(document).on('keyup', '.searchInput', function(event) {
        event.preventDefault();
        var thisInput = $(this);
        var thisForm = $(this).closest('form');
        var formAction = thisForm.attr('action');
        var formMethod = thisForm.attr('method');
        var formData = thisForm.serialize();
        $.ajax({
            url: formAction,
            type: formMethod,
            dataType: 'json',
            data: formData,
            success: function(data) {
                $('.data').html(data);
            },
        });
        return false;

    });
/*********************************** A ********************************************/
/***************************************************************************************/
  function createUUID() {
    // http://www.ietf.org/rfc/rfc4122.txt
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 11; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[5] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[4] = hexDigits.substr((s[4] & 0x3) | 0x2, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[3] = s[7] = "-";

    var uuid = s.join("");
    return uuid;
}
 $('.account_number').val(Date.now());
$('.ajax-form-request-client').on('submit',function(event){
        event.preventDefault();
        $('.ajaxMessage').remove();
        $('*').removeClass('has-error');

        var thisForm   = $(this) ;
        var formAction = thisForm.attr('action');
        var formMethod = thisForm.attr('method');
        // var formData   =  thisForm.serialize();

        var formData    = new FormData(this);
        // formData.append('file', file_data);
        $.ajax({
            url: formAction,
            type: formMethod,
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $(thisForm).find('#submit').append(' <i class="fa fa-spinner fa-pulse"></i> ').attr('disabled','disabled');

            },
            success: function(data){
                if(data.status == 'true')
                {
                    thisForm.parent().prepend('<div class="panel bg-success pos-rlt"><span class="arrow bottom b-success"></span><div class="panel-body">'
                                  +data.message+'</div></div>');
                    $(thisForm).trigger("reset");
                 $('.account_number').val(createUUID());
                }
                else
                {
                    thisForm.parent().prepend('<div class="alert alert-danger ajaxMessage">'+data.message+'</div>');
                }
            },
            error:function(data){
                var errors = data.responseJSON;
                $.each(errors, function(index, val) {
                     index = index.replace('.*','[]')
                     $(thisForm).find('[name='+''+index+''+']').parent().addClass('has-error');
                     $(thisForm).find('[name='+''+index+''+']').parent().append('<small class="text-danger ajaxMessage">'+val+'</small>');
                });

            },
            complete: function(){
                $(thisForm).find('#submit').find('.fa').remove();
                $(thisForm).find('#submit').prop('disabled',false);
            },

        });//ajax

        return false;


    });//ajax-form-request




/*********************************************************************************/
});//ready

