@extends('admin.layouts.index')

@section('content')

<div class="row">
    <div class="col-md-12">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div>
            @endif

            @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
        <form method="POST" action="{{route('clients.store')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <h3 class="page-title"> إضافة عميل جديد </h3>
            <hr>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label> الأسم </label>
                        <input class="form-control" type="text" name="name">
                    </div>
                </div>
                <div class="col-sm-3">
                        <label>الصورة </label>
                        <input type="file" class="form-control" name="image">
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Client ID</label>
                        <input class="form-control" name="client_id" type="text">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>الأيميل</label>
                        <input class="form-control" type="email" name="email">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>رقم التليفون</label>
                        <input class="form-control" type="text" name="phone">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>الوظيفة</label>
                        <input class="form-control" name="job_title" type="text">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>أسم الشركة </label>
                        <input class="form-control" type="text" name="company_name">
                    </div>
                </div>
            </div>
            <div class="submit-section">
                <button class="btn btn-primary submit-btn">حفظ</button>
            </div>
        </form>
    </div>
</div>

@endsection
