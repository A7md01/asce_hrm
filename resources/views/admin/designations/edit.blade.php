@extends('admin.layouts.index')

@section('content')

<div class="row">
    <div class="col-md-12">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div>
            @endif

            @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
            <form method="POST" action="{{route('designations.update' , $designations->id)}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="put">
            <h3 class="page-title">تعديل الوظيفة </h3>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label style="font-size: 120%">المُسمى الوظيفى </label>
                    <input class="form-control" type="text" name="name" value="{{$designations->name}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label style="font-size: 120%">القسم</label>
                        <select class="form-control select" name="department_id">
                            @foreach ($designations as $designation)
                                <option>{{ $designation->department->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="submit-section">
                <button class="btn btn-primary submit-btn">Save</button>
            </div>
        </form>
    </div>
</div>

@endsection
